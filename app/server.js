const pg = require("pg");
const express = require("express");
const app = express();

const port = 3000; 
const hostname = "localhost";

const env = require("../env.json");
const Pool = pg.Pool;
const pool = new Pool(env);
pool.connect().then(function () {
    console.log(`Connected to database ${env.database}`);
});

app.use(express.static("public_html"));
app.use(express.json());


app.get("/", function (req, response){
	let last_name = req.query.last_name;
	let sel_que = `SELECT * FROM pt_data where pat_last_name = '${last_name}'`;
	console.log(sel_que);
	pool.query(sel_que, (err, res) => {
		if (err) {
			console.log('SELECT error: ', err);
		}
		if (res) {
			let results = res;
			console.log(results);
			let pat_info = JSON.stringify(results.rows);
			console.log(pat_info);
			response.send(results);
		}
	});
});

app.post("/", function (req, res){
	let dataGood = true; 
	let data = req.body.data;
	data.bio_mom_concep_age = parseInt(data.bio_mom_concep_age);
	data.bio_mom_prev_births_num = parseInt(data.bio_mom_prev_births_num);
	if (!data.user_first_name || (data.user_first_name.length > 20)
		|| !data.user_last_name || (data.user_last_name.length > 20)
		|| !data.user_rel || !data.user_email || !data.user_phone
		|| !data.pat_first_name || (data.pat_first_name.length > 20)
		|| !data.pat_last_name || (data.pat_last_name.length > 20)
		|| (data.pat_midd_name.length > 20) || !data.pat_sex 
		|| !data.pat_birth_date){
		dataGood = false;
	}
	console.log(data);
	let holder = String(data.user_first_name);
	//dataGood = true; WHEN TESTING, uncomment for faster action.
	if (dataGood === true){
		addPatient(data);
	}
	else{
		console.log("bad patient data");
	}

});

function addPatient(data){
	pool.query(
		`INSERT INTO pt_data (user_first_name, user_last_name, user_rel, 
		user_other_rel, user_email, user_phone, pat_first_name, pat_midd_name, 
		pat_last_name, pat_sex, pat_birth_date, pat_state, pat_county, 
		bio_mom_injury, bio_mom_injury_details, bio_mom_hosp, bio_mom_hop_details, 
		bio_mom_xrays, bio_mom_xrays_details, bio_mom_meds, bio_mom_meds_details, 
		bio_mom_diet, bio_mom_concep_age, bio_mom_prev_births, 
		bio_mom_prev_births_num, bio_mom_mat_care, fetus_activity_level, 
		fet_act_lev_change, bio_mom_preg_emos, bio_mom_preg_infects, 
		bio_mom_preg_premature, bio_mom_preg_rash, bio_mom_preg_bedrest, 
		bio_mom_preg_toxemia, bio_mom_preg_diffconc, bio_mom_preg_anemia, 
		bio_mom_preg_35lb, bio_mom_preg_swell, bio_mom_preg_vagblood, 
		bio_mom_preg_measles, bio_mom_preg_nausea, bio_mom_preg_flu, 
		bio_mom_preg_hbp, bio_mom_preg_kidney, bio_mom_preg_strep, 
		bio_mom_preg_miscrisk, bio_mom_preg_rh, bio_mom_preg_headache, 
		bio_mom_preg_cold, bio_mom_preg_urine, bio_mom_preg_virus, 
		bio_mom_preg_other)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, 
		$14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, 
		$27, $28, $29, $30, $31, $32, $33, $34, $35, $36, $37, $38, $39, 
		$40, $41, $42, $43, $44, $45, $46, $47, $48, $49, $50, $51, $52
		) RETURNING *`,
		[data.user_first_name, data.user_last_name, data.user_rel,
		data.user_other_rel, data.user_email, data.user_phone, data.pat_first_name, 
		data.pat_midd_name, data.pat_last_name, data.pat_sex, 20150531, 
		data.pat_state, data.pat_county, data.bio_mom_injury, 
		data.bio_mom_injury_details, data.bio_mom_hosp, data.bio_mom_hop_details, 
		data.bio_mom_xrays, data.bio_mom_xrays_details, data.bio_mom_meds, 
		data.bio_mom_meds_details, data.bio_mom_diet, data.bio_mom_concep_age, 
		data.bio_mom_prev_births, data.bio_mom_prev_births_num, data.bio_mom_mat_care, 
		data.fetus_activity_level, data.fet_act_lev_change, data.bio_mom_preg_emos, 
		data.bio_mom_preg_infects, data.bio_mom_preg_premature, data.bio_mom_preg_rash, 
		data.bio_mom_preg_bedrest, data.bio_mom_preg_toxemia, 
		data.bio_mom_preg_diffconc, data.bio_mom_preg_anemia, data.bio_mom_preg_35lb, 
		data.bio_mom_preg_swell, data.bio_mom_preg_vagblood, data.bio_mom_preg_measles, 
		data.bio_mom_preg_nausea, data.bio_mom_preg_flu, data.bio_mom_preg_hbp, 
		data.bio_mom_preg_kidney, data.bio_mom_preg_strep, data.bio_mom_preg_miscrisk, 
		data.bio_mom_preg_rh, data.bio_mom_preg_headache, data.bio_mom_preg_cold, 
		data.bio_mom_preg_urine, data.bio_mom_preg_virus, data.bio_mom_preg_other]
	).then(function (response) {
		console.log("Inserted: ");
		console.log(response.rows);
	}).catch(function (error){
		console.log(error);
	});	
}
app.listen(port, hostname, () => {
    console.log(`Listening at: http://${hostname}:${port}`);
});
