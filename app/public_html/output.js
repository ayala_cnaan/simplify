console.log("starting output");

//const data = new Object();
//const { jsPDF } = require("jspdf");
//const doc = new jsPDF();

let access_last_name = "";
let chosen_opt = "";
let info = [];

let submitButt = document.getElementById("submit1");
let opt1Butt = document.getElementById("option1");
let opt2Butt = document.getElementById("option2");
let opt3Butt = document.getElementById("option3");
let opt1pdfButt = document.getElementById("pdfopt1");
let opt2pdfButt = document.getElementById("pdfopt2");
let opt3pdfButt = document.getElementById("pdfopt3");

submitButt.addEventListener("click", ()=>{
	access_last_name = document.getElementById("last_name").value;
	if (!(access_last_name === "") && !(chosen_opt === "")){
		
		let url = `/?last_name=${access_last_name}`;
		console.log("Sending url: ", url);
		fetch(url).then(function(response){
			return response.json();
		}).then(function (data){
			info = data.rows[0];
			console.log(info);
			
			if (access_last_name === ""){
				document.getElementById("out0").style.display = "block";
				document.getElementById("out1").style.display = "none";
				document.getElementById("out2").style.display = "none";
				document.getElementById("out3").style.display = "none";
			}
			else {
				if (chosen_opt === "opt_1"){
					emptyOpt1();
					fillInOpt1(info);
					document.getElementById("out0").style.display = "none";
					document.getElementById("out2").style.display = "none";
					document.getElementById("out3").style.display = "none";
					document.getElementById("out1").style.display = "block";
				}
				else if (chosen_opt === "opt_2"){
					emptyOpt2();
					fillInOpt2(info);
					document.getElementById("out0").style.display = "none";
					document.getElementById("out1").style.display = "none";
					document.getElementById("out3").style.display = "none";
					document.getElementById("out2").style.display = "block";
				}
				else if (chosen_opt === "opt_3"){
					emptyOpt3();
					fillInOpt3(info);
					document.getElementById("out0").style.display = "none";
					document.getElementById("out1").style.display = "none";
					document.getElementById("out2").style.display = "none";
					document.getElementById("out3").style.display = "block";
				}
				else {
					document.getElementById("out0").style.display = "block";
					document.getElementById("out1").style.display = "none";
					document.getElementById("out2").style.display = "none";
					document.getElementById("out3").style.display = "none";
				}
			}
		}).catch(function(error){
			document.getElementById("out0").style.display = "block";
			document.getElementById("out1").style.display = "none";
			document.getElementById("out2").style.display = "none";
			document.getElementById("out3").style.display = "none";
		});
	}


});

function emptyNode(curr_node){
	while (curr_node.hasChildNodes()){
		curr_node.removeChild(curr_node.childNodes[0]);
	}
}

function emptyOpt3(){
	let opt3birthdate = document.getElementById("opt3pat_birthdate");
	emptyNode(opt3birthdate);
	let opt3patname = document.getElementById("opt3pat_name");
	emptyNode(opt3patname);
	let opt3patsex = document.getElementById("opt3pat_sex");
	emptyNode(opt3patsex);
	let opt3preginj = document.getElementById("opt3mat_preg_inj");
	emptyNode(opt3preginj);
	let opt3pregxray = document.getElementById("opt3mat_preg_xray");
	emptyNode(opt3pregxray);
	let opt3pregcare = document.getElementById("opt3mat_preg_pre_care");
	emptyNode(opt3pregcare);
	let opt3pregdiet = document.getElementById("opt3mat_preg_diet");
	emptyNode(opt3pregdiet);
	let opt3pregbed = document.getElementById("opt3mat_preg_bed");
	emptyNode(opt3pregbed);
	let opt3pregmeds = document.getElementById("opt3mat_preg_meds");
	emptyNode(opt3pregmeds);
}

function fillInOpt3(data){
	console.log("option 3 loading");
	let bd_text = data.pat_birth_date;
	let bd_day = bd_text.substring(6,8);
	let bd_month = bd_text.substring(4,6);
	let bd_year = bd_text.substring(0,4);
	let new_bd = bd_month + "/" + bd_day + "/" + bd_year;
	document.getElementById("opt3pat_birthdate").appendChild(document.createTextNode("Patient Birthdate: " + new_bd));
	let pat_name = data.pat_first_name + " " + data.pat_last_name;
	document.getElementById("opt3pat_name").appendChild(document.createTextNode("Patient Name: " +pat_name));
	let pat_sex = data.pat_sex;
	document.getElementById("opt3pat_sex").appendChild(document.createTextNode("Patient Sex: "+pat_sex));
	let preg_inj_cell = document.getElementById("opt3mat_preg_inj");
	if (data.bio_mom_injury === true){
		preg_inj_cell.appendChild(document.createTextNode('\u25A0'));
	}else{
		preg_inj_cell.appendChild(document.createTextNode('\u25A1'));
	}
	let preg_xray_cell = document.getElementById("opt3mat_preg_xray");
	if (data.bio_mom_xrays === true){
		preg_xray_cell.appendChild(document.createTextNode('\u25A0'));
	}else{
		preg_xray_cell.appendChild(document.createTextNode('\u25A1'));
	}
	let preg_precare_cell = document.getElementById("opt3mat_preg_pre_care");
	if (data.bio_mom_mat_care === true){
		preg_precare_cell.appendChild(document.createTextNode('\u25A0'));
	}else{
		preg_precare_cell.appendChild(document.createTextNode('\u25A1'));
	}
	let preg_diet_cell = document.getElementById("opt3mat_preg_diet");
	if (data.bio_mom_diet === true){
		preg_diet_cell.appendChild(document.createTextNode('\u25A0'));
	}else{
		preg_diet_cell.appendChild(document.createTextNode('\u25A1'));
	}
	let preg_bedrest_cell = document.getElementById("opt3mat_preg_bed");
	if (data.bio_mom_preg_bedrest === true){
		preg_bedrest_cell.appendChild(document.createTextNode('\u25A0'));
	}else{
		preg_bedrest_cell.appendChild(document.createTextNode('\u25A1'));
	}
	let preg_meds_cell = document.getElementById("opt3mat_preg_meds");
	if (data.bio_mom_meds === true){
		preg_meds_cell.appendChild(document.createTextNode('\u25A0'));
	}else{
		preg_meds_cell.appendChild(document.createTextNode('\u25A1'));
	}
}

function emptyOpt2(){
	opt2nodes = ["out2pat_last_name", "out2pat_first_name", "out2pat_middle_name", "out2mom_rh",
	"out2mom_anemia", "out2mom_bedrest", "out2mom_emo_probs", "out2mom_ex_swell", "out2mom_ex_vom", 
	"out2mom_ex_weight", "out2mom_ex_weight", "out2mom_flu", "out2mom_headaches", "out2mom_hbp", 
	"out2mom_infections", "out2mom_kidney", "out2mom_measles", "out2mom_pre_labor", "out2mom_rashes", 
	"out2mom_cold", "out2mom_strep", "out2mom_toxemia", "out2mom_urine", "out2mom_vag_bleed"
	];
	for (let i = 0; i < opt2nodes.length; i++){
		emptyNode(document.getElementById(opt2nodes[i]));
	}
}

function fillInOpt2(data){
	console.log("option 2 loading");
	let last_name_cell = document.getElementById("out2pat_last_name");
	let last_name_val = document.createTextNode(data.pat_last_name);
	last_name_cell.appendChild(last_name_val);
	let first_name_cell = document.getElementById("out2pat_first_name");
	let first_name_val = document.createTextNode(data.pat_first_name);
	first_name_cell.appendChild(first_name_val);
	let middle_name_cell = document.getElementById("out2pat_middle_name");
	let middle_name_val = document.createTextNode(data.pat_midd_name);
	middle_name_cell.appendChild(middle_name_val);
	if (data.bio_mom_preg_anemia === true){
	document.getElementById("out2mom_anemia").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_anemia").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_bedrest === true){
	document.getElementById("out2mom_bedrest").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_bedrest").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_emos === true){
	document.getElementById("out2mom_emo_probs").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_emo_probs").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_swell === true){
	document.getElementById("out2mom_ex_swell").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_ex_swell").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_nausea === true){
	document.getElementById("out2mom_ex_vom").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_ex_vom").appendChild(document.createTextNode("no"));}

	if (data.bio_mom_preg_35lb === true){
	document.getElementById("out2mom_ex_weight").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_ex_weight").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_flu === true){
	document.getElementById("out2mom_flu").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_flu").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_headache === true){
	document.getElementById("out2mom_headaches").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_headaches").appendChild(document.createTextNode("no"));}
	if (data.bio_bio_mom_preg_hbp === true){
	document.getElementById("out2mom_hbp").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_hbp").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_infects === true){
	document.getElementById("out2mom_infections").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_infections").appendChild(document.createTextNode("no"));}

	if (data.bio_mom_preg_kidney === true){
	document.getElementById("out2mom_kidney").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_kidney").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_measles === true){
	document.getElementById("out2mom_measles").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_measles").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_premature === true){
	document.getElementById("out2mom_pre_labor").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_pre_labor").appendChild(document.createTextNode("no"));}
	if (data.bio_bio_mom_preg_rash === true){
	document.getElementById("out2mom_rashes").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_rashes").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_rh === true){
	document.getElementById("out2mom_rh").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_rh").appendChild(document.createTextNode("no"));}

	if (data.bio_mom_preg_cold === true){
	document.getElementById("out2mom_cold").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_cold").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_strep === true){
	document.getElementById("out2mom_strep").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_strep").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_toxemia === true){
	document.getElementById("out2mom_toxemia").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_toxemia").appendChild(document.createTextNode("no"));}
	if (data.bio_bio_mom_preg_urine === true){
	document.getElementById("out2mom_urine").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_urine").appendChild(document.createTextNode("no"));}
	if (data.bio_mom_preg_vagblood === true){
	document.getElementById("out2mom_vag_bleed").appendChild(document.createTextNode("yes"));}
	else{
	document.getElementById("out2mom_vag_bleed").appendChild(document.createTextNode("no"));}
}

function emptyOpt1(){
	let opt1nodes = ["out1pat_name", "out1pat_birth_date", "out1user_name", "out1user_rel_pat", 
	"out1user_phone", "out1user_email", "preg_act_level1", "preg_act_lev_change", "bir_mom_prev_pregs"];
	for (let i = 0; i <opt1nodes.length; i++){
		emptyNode(document.getElementById(opt1nodes[i]));
	}
}


function fillInOpt1(data){
	console.log("option 1 loading");
	let name_cell = document.getElementById("out1pat_name");
	let pat_full_name = document.createTextNode(data.pat_first_name + " " + data.pat_last_name); 
	name_cell.appendChild(pat_full_name);
	let birth_date_cell = document.getElementById("out1pat_birth_date");
	let birth_date = document.createTextNode(data.pat_birth_date);
	birth_date_cell.appendChild(birth_date);
	let contact_cell = document.getElementById("out1user_name");
	let user_full_name = document.createTextNode(data.user_first_name + " " + data.user_last_name);
	contact_cell.appendChild(user_full_name);
	let rel_cell = document.getElementById("out1user_rel_pat");
	let rel_val = null;
	if (data.user_rel === "other"){
		rel_val = document.createTextNode(data.user_rel + ": " + data.user_other_rel);
	}else{
		rel_val = document.createTextNode(data.user_rel);
	}
	rel_cell.appendChild(rel_val);
	let phone_cell = document.getElementById("out1user_phone");
	let phone_val = document.createTextNode(data.user_phone);
	phone_cell.appendChild(phone_val);
	let email_cell = document.getElementById("out1user_email");
	let email_val = document.createTextNode(data.user_email);
	email_cell.appendChild(email_val);
	let preg_act_lev_cell = document.getElementById("preg_act_level1");
	let fet_hold = "";
	if (!(data.fetus_activity_level === null)){
		fet_hold = data.fetus_activity_level;}
	let preg_act_lev_val = document.createTextNode("Pregnancy activity level: " + fet_hold);
	preg_act_lev_cell.appendChild(preg_act_lev_val);
	let preg_lev_ch_cell = document.getElementById("preg_act_lev_change");
	let act_ch = "no";
	if (data.fet_act_lev_change === true) {act_ch = "yes";}
	let preg_lev_ch_val = document.createTextNode("Change in activity level during pregnancy: " + act_ch);
	preg_lev_ch_cell.appendChild(preg_lev_ch_val);
	let preg_num_cell = document.getElementById("bir_mom_prev_pregs");
	let preg_num = 0;
	if (data.bio_mom_prev_births === true){preg_num = data.bio_mom_prev_births_num;}
	let preg_num_val = document.createTextNode("Previous pregnancies: "+preg_num);
	preg_num_cell.appendChild(preg_num_val);
}

opt1Butt.addEventListener("click", ()=>{
	chosen_opt = "opt_1";
});
opt2Butt.addEventListener("click", ()=>{
	chosen_opt = "opt_2";
});
opt3Butt.addEventListener("click", ()=>{
	chosen_opt = "opt_3";
});

let element1 = document.getElementById("out1inner");
let optstyle1 ={
	margin: 5,
	filename: 'option1.pdf'
};
opt1pdfButt.addEventListener("click", ()=>{
	html2pdf(element1, optstyle1);
});

let element2 = document.getElementById("out2inner");
let optstyle2 ={
	margin: 5,
	filename: 'option2.pdf'
};
opt2pdfButt.addEventListener("click", ()=>{
	html2pdf(element2, optstyle2);
});

let element3 = document.getElementById("out3inner");
let optstyle3 ={
	margin: 5,
	filename: 'option3.pdf'
};
opt3pdfButt.addEventListener("click", ()=>{
	html2pdf(element3, optstyle3);
});
