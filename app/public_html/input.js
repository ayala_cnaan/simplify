console.log("starting");

//variables for collected information and whether it is ready to go. 
const data = new Object(); let dataGood = true;

//variables for collecting the user' information
let user_first_name = ""; let user_last_name = ""; let user_rel = ""; 
let user_other_rel = ""; let user_email = ""; let user_phone = ""; 

//variables for collecting information on the patient
let pat_first_name = ""; let pat_midd_name = ""; let pat_last_name = "";
let pat_sex = ""; let pat_birth_date = ""; let pat_state = "";
let pat_county = "";

//variables for collecting data on biological mother
let bio_mom_injury = false; let bio_mom_injury_details = "";
let bio_mom_hosp = false; let bio_mom_hop_details = "";
let bio_mom_xrays = false; let bio_mom_xrays_details = "";
let bio_mom_meds = false; let bio_mom_meds_details = "";
let bio_mom_diet = false; let bio_mom_concep_age = 0;
let bio_mom_prev_births = false; let bio_mom_prev_births_num = 0;
let bio_mom_mat_care = false; let fetus_activity_level = null;
let fet_act_lev_change = false;

//buttons for various functions, some radio
let submitButt = document.getElementById("submit");
let otherUserButt = document.getElementById("user_rel_other");
let selfUserButt = document.getElementById("user_rel_self");
let parentUserButt = document.getElementById("user_rel_parent");
let otherDef = document.getElementById("if_user_other");
let matInjuryButt = document.getElementById("bio_mom_injury");
let matHospButt = document.getElementById("bio_mom_hosp");
let matXraysButt = document.getElementById("bio_mom_xrays");
let matDietButt = document.getElementById("bio_mom_diet");
let matMedsButt = document.getElementById("bio_mom_meds");
let matPrevPregsButt = document.getElementById("bio_mom_prev_births_yes");
let matPrevPregsButt2 = document.getElementById("bio_mom_prev_births_no");
let matPrevMatCare = document.getElementById("bio_mom_pre_yes");
let fetVeryAct = document.getElementById("fetus_very_active");
let fetAct = document.getElementById("fetus_active");
let fetQuiet = document.getElementById("fetus_quiet");
let fetActChange = document.getElementById("fet_act_change_yes");

//variables for various biological mother's conditions
let bio_mom_preg_emos = false; let bio_mom_preg_infects = false; 
let bio_mom_preg_premature = false; let bio_mom_preg_rash = false;
let bio_mom_preg_bedrest = false; let bio_mom_preg_toxemia = false; 
let bio_mom_preg_diffconc = false; let bio_mom_preg_anemia = false; 
let bio_mom_preg_35lb = false; let bio_mom_preg_swell = false; 
let bio_mom_preg_vagblood = false; let bio_mom_preg_measles = false; 
let bio_mom_preg_nausea = false; let bio_mom_preg_flu = false; 
let bio_mom_preg_hbp = false; let bio_mom_preg_kidney = false; 
let bio_mom_preg_strep = false; let bio_mom_preg_miscrisk = false; 
let bio_mom_preg_rh = false; let bio_mom_preg_headache = false; 
let bio_mom_preg_cold = false; let bio_mom_preg_urine = false; 
let bio_mom_preg_virus = false; let bio_mom_preg_other = false; 

//Main driver of events, the submit button function
submitButt.addEventListener("click", ()=>{
	dataGood = true;
	resetAllValue();
	checkUserInfo();
	checkPatBasicInfo();
	matPregInfo();
	pregTabInfo();
	console.log(data);
	
	//REMOVE LINE below
	dataGood = true;
	if (dataGood){
		fetch('/',{
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({data})
		}).then(function (response){
			let resp = response.status;
			console.log(resp);
			//alert("You have successfully uploaded information!");
		}).catch(function (error){
			//alert("Something went wrong, please try again.");
		console.log(error);});
	}
	else{
		alert("Some data missing, try again.");
	}
});

//This needs to be abstracted out, it's clunky code.
function pregTabInfo(){
	if (document.getElementById("bio_mom_preg_emos").checked){
		bio_mom_preg_emos = true;
	}
	if (document.getElementById("bio_mom_preg_infects").checked){
		bio_mom_preg_infects = true;
	}
	if (document.getElementById("bio_mom_preg_premature").checked){
		bio_mom_preg_premature = true;
	}
	if (document.getElementById("bio_mom_preg_rash").checked){
		bio_mom_preg_rash = true;
	}
	if (document.getElementById("bio_mom_preg_bedrest").checked){
		bio_mom_preg_bedrest = true;
	}
	if (document.getElementById("bio_mom_preg_toxemia").checked){
		bio_mom_preg_toxemia = true;
	}
	if (document.getElementById("bio_mom_preg_diffconc").checked){
		bio_mom_preg_diffconc = true;
	}
	if (document.getElementById("bio_mom_preg_anemia").checked){
		bio_mom_preg_anemia = true;
	}
	if (document.getElementById("bio_mom_preg_35lb").checked){
		bio_mom_preg_35lb = true;
	}
	if (document.getElementById("bio_mom_preg_swell").checked){
		bio_mom_preg_swell = true;
	}
	if (document.getElementById("bio_mom_preg_vagblood").checked){
		bio_mom_preg_vagblood = true;
	}
	if (document.getElementById("bio_mom_preg_measles").checked){
		bio_mom_preg_measles = true;
	}
	if (document.getElementById("bio_mom_preg_nausea").checked){
		bio_mom_preg_nausea = true;
	}
	if (document.getElementById("bio_mom_preg_flu").checked){
		bio_mom_preg_flu = true;
	}
	if (document.getElementById("bio_mom_preg_hbp").checked){
		bio_mom_preg_hbp = true; 
	}
	if (document.getElementById("bio_mom_preg_kidney").checked){
		bio_mom_preg_kidney = true;
	}
	if (document.getElementById("bio_mom_preg_strep").checked){
		bio_mom_preg_strep = true; 
	}
	if (document.getElementById("bio_mom_preg_miscrisk").checked){
		bio_mom_preg_miscrisk = true;
	}
	if (document.getElementById("bio_mom_preg_rh").checked){
		bio_mom_preg_rh = true;
	}
	if (document.getElementById("bio_mom_preg_headache").checked){
		bio_mom_preg_headache = true;
	}
	if (document.getElementById("bio_mom_preg_cold").checked){
		bio_mom_preg_cold = true;
	}
	if (document.getElementById("bio_mom_preg_urine").checked){
		bio_mom_preg_urine = true;
	}
	if (document.getElementById("bio_mom_preg_virus").checked){
		bio_mom_preg_virus = true;
	}
	if (document.getElementById("bio_mom_preg_other").checked){
		bio_mom_preg_other = true;
	}

	data.bio_mom_preg_emos = bio_mom_preg_emos;
	data.bio_mom_preg_infects = bio_mom_preg_infects;
	data.bio_mom_preg_premature = bio_mom_preg_premature;
	data.bio_mom_preg_rash = bio_mom_preg_rash; 
	data.bio_mom_preg_bedrest = bio_mom_preg_bedrest
	data.bio_mom_preg_toxemia = bio_mom_preg_toxemia;
	data.bio_mom_preg_diffconc = bio_mom_preg_diffconc;
	data.bio_mom_preg_anemia = bio_mom_preg_anemia;
	data.bio_mom_preg_35lb = bio_mom_preg_35lb;
	data.bio_mom_preg_swell = bio_mom_preg_swell; 
	data.bio_mom_preg_vagblood = bio_mom_preg_vagblood;
	data.bio_mom_preg_measles = bio_mom_preg_measles; 
	data.bio_mom_preg_nausea = bio_mom_preg_nausea; 
	data.bio_mom_preg_flu = bio_mom_preg_flu; 
	data.bio_mom_preg_hbp = bio_mom_preg_hbp; 
	data.bio_mom_preg_kidney = bio_mom_preg_kidney; 
	data.bio_mom_preg_strep = bio_mom_preg_strep; 
	data.bio_mom_preg_miscrisk = bio_mom_preg_miscrisk; 
	data.bio_mom_preg_rh = bio_mom_preg_rh; 
	data.bio_mom_preg_headache = bio_mom_preg_headache; 
	data.bio_mom_preg_cold = bio_mom_preg_cold; 
	data.bio_mom_preg_urine = bio_mom_preg_urine; 
	data.bio_mom_preg_virus = bio_mom_preg_virus; 
	data.bio_mom_preg_other = bio_mom_preg_other; 
}

//This needs to be abstracted out, it's clunky code.
function matPregInfo(){
	if (matInjuryButt.checked){
		bio_mom_injury = true;
		bio_mom_injury_details = document.getElementById("maternal_injury").value;
	}else{
		bio_mom_injury = false; bio_mom_injury_details = "";
		document.getElementById("if_maternal_injury").style.display="none";
	}
	if (matHospButt.checked){
		bio_mom_hosp = true; 
		bio_mom_hop_details = document.getElementById("maternal_hosp").value;
	}else{
		bio_mom_hosp = false; bio_mom_hop_details = "";
		document.getElementById("if_mom_hosp").style.display="none";
	}
	if (matXraysButt.checked){
		bio_mom_xrays = true;
		bio_mom_xrays_details = document.getElementById("maternal_xrays").value;
	}else{
		bio_mom_xrays = false; bio_mom_xrays_details = "";
		document.getElementById("if_mom_xrays").style.display="none";
	}
	if (matDietButt.checked){
		bio_mom_diet = true;
	}else{
		bio_mom_diet = false;
	}
	if (matMedsButt.checked){
		bio_mom_meds = true;
		bio_mom_meds_details = document.getElementById("maternal_meds").value;
	}else{
		bio_mom_meds = false; bio_mom_meds_details = "";
		document.getElementById("if_mom_meds").style.display="none";
	}
	if (matPrevPregsButt.checked){
		bio_mom_prev_births = true;
		bio_mom_prev_births_num = document.getElementById("maternal_prev_pregs").value;
	}else{
		bio_mom_prev_births = false; bio_mom_prev_births_num = null;
		document.getElementById("if_prev_pregs").style.display="none";
	}
	if (matPrevMatCare.checked){
		bio_mom_mat_care = true;
	}else{
		bio_mom_mat_care = false;
	}
	if (fetVeryAct.checked){
		fetus_activity_level = "very active";
	}else if(fetAct.checked){
		fetus_activity_level = "active";
	}else if(fetQuiet.checked){
		fetus_activity_level = "quiet";
	}else{
		fetus_activity_level = null;
	}
	if (fetActChange.checked){
		fet_act_lev_change = true;
	}else{
		fet_act_lev_change = false;
	}
	
	data.bio_mom_injury = bio_mom_injury; 
	data.bio_mom_injury_details = bio_mom_injury_details; 
	data.bio_mom_hosp = bio_mom_hosp;
	data.bio_mom_hop_details = bio_mom_hop_details; 
	data.bio_mom_xrays = bio_mom_xrays; 
	data.bio_mom_xrays_details = bio_mom_xrays_details; 
	data.bio_mom_diet = bio_mom_diet; 
	data.bio_mom_meds = bio_mom_meds; 
	data.bio_mom_meds_details = bio_mom_meds_details; 
	data.bio_mom_concep_age = parseInt(document.getElementById("bio_mom_concep_age").value);
	if (data.bio_mom_concep_age == NaN){
		data.bio_mom_concep_age = 0;
	}
	data.bio_mom_prev_births = bio_mom_prev_births; 
	data.bio_mom_prev_births_num = bio_mom_prev_births_num; 
	if (data.bio_mom_prev_births_num == NaN){
		data.bio_mom_prev_births_num = 0;
	}
	data.bio_mom_mat_care = bio_mom_mat_care; 
	data.fetus_activity_level = fetus_activity_level; 
	data.fet_act_lev_change = fet_act_lev_change; 
}

//This needs to be abstracted out, it's clunky code.
function checkPatBasicInfo(){
	pat_first_name = document.getElementById("pat_first_name").value;
	if (pat_first_name.length < 1 || pat_first_name > 20){
		alert("First name of patient is either missing or too long.");
		dataGood = false;
	}else{dataGood = true;}
	pat_midd_name = document.getElementById("pat_middle_name").value;
	if (pat_midd_name > 20){
		alert("Middle name of patient too long.");
		dataGood = false;
	}else{dataGood = true;}
	pat_last_name = document.getElementById("pat_last_name").value;
	if (pat_last_name.length < 1 || pat_last_name > 20) {
		alert("Last name of patient is either missing or too long.");
		dataGood = false;
	}else{dataGood = true;}
	pat_sex = document.getElementById("pat_sex").value;
	if (pat_sex === ""){
		alert ("Please select patient sex.");
		dataGood = false;
	}else{dataGood = true;}
	const dateformat = /^\d{4}\-\d{2}\-\d{2}$/;
	pat_birth_date = document.getElementById("pat_birthdate").value;
	if (!pat_birth_date.match(dateformat)){
		alert ("Please provide patient birthdate.");
		dataGood = false;
	}else{dataGood = true;}
	pat_state = document.getElementById("pat_state").value;
	pat_county = document.getElementById("pat_county").value;
	
	data.pat_first_name = pat_first_name;
	data.pat_midd_name = pat_midd_name; 
	data.pat_last_name = pat_last_name; 
	data.pat_sex = pat_sex; data.pat_birth_date = pat_birth_date; 
	data.pat_state = pat_state; data.pat_county = pat_county; 
}

function checkUserInfo(){
	user_first_name = document.getElementById("first_name_user").value;
	if (user_first_name.length < 1 || user_first_name.length > 20) {
		alert("First name of user is either missing or too long.")
		dataGood = false;
	}else{dataGood=true;}
	user_last_name = document.getElementById("last_name_user").value;
	if (user_last_name.length < 1 || user_last_name.length > 20) {
		alert("Last name of the user is either missing or too long.")
		dataGood = false;
	}else{dataGood=true;}
	if (parentUserButt.checked){user_rel = "parent"; dataGood = true;}
	else if (selfUserButt.checked){user_rel = "self"; dataGood = true;}
	else if (otherUserButt.checked){user_rel = "other"; dataGood = true;}
	else{
		alert ("Please provide relationship status.");
		dataGood = false;
	}
	console.log(user_rel);
	if (user_rel === "other"){
		user_other_rel = document.getElementById("user_other_rel").value;
	}
	else {user_other_rel = "";}
	user_email = document.getElementById("user_email").value;
	const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if (!user_email.match(mailformat)){
		alert("Email address either missing or incorrect.");
		dataGood = false;
	}else{dataGood=true;}
	user_phone = document.getElementById("user_phone").value;
	const phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
	if (!user_phone.match(phoneno)){
		alert("Phone number either missing or in incorrect format.");
		dataGood = false;
	}else{dataGood=true;}
	
	data.user_first_name = user_first_name;
	data.user_last_name = user_last_name;
	data.user_rel = user_rel; data.user_other_rel = user_other_rel; 
	data.user_email = user_email; data.user_phone = user_phone;
}


matPrevPregsButt2.addEventListener("click", ()=>{
	document.getElementById("if_prev_pregs").style.display="none";
});

matPrevPregsButt.addEventListener("click", ()=>{
	document.getElementById("if_prev_pregs").style.display="block";
});

matMedsButt.addEventListener("click", ()=>{
	document.getElementById("if_mom_meds").style.display="block";
});

matXraysButt.addEventListener("click", ()=>{
	document.getElementById("if_mom_xrays").style.display="block";
});

matHospButt.addEventListener("click", ()=>{
	document.getElementById("if_mom_hosp").style.display="block";
	bio_mom_hosp = true;
});

matInjuryButt.addEventListener("click", ()=>{
	document.getElementById("if_maternal_injury").style.display="block";
});

otherUserButt.addEventListener("click", ()=>{
	otherDef.style.display="block";
	user_rel="other";
});
selfUserButt.addEventListener("click", ()=>{
	otherDef.style.display="none";
	user_rel="self";
});
parentUserButt.addEventListener("click", ()=>{
	otherDef.style.display="none";
	user_rel="parent";
});

function resetAllValue(){
	//user data reset
	user_first_name = ""; user_last_name = ""; user_rel = ""; 
	user_other_rel = ""; user_email = ""; user_phone = ""; 
	//patient basic info reset
	pat_first_name = ""; pat_midd_name = ""; pat_last_name = "";
	pat_sex = ""; pat_birth_date = ""; pat_state = "";
	pat_county = "";
	//biological mother pregnancy info
	bio_mom_injury = false; bio_mom_injury_details = "";
	bio_mom_hosp = false; bio_mom_hop_details = "";
	bio_mom_xrays = false; bio_mom_xrays_details = "";
	bio_mom_meds = false; bio_mom_meds_details = "";
	bio_mom_diet = false; bio_mom_concep_age = 0;
	bio_mom_mat_care = false; fetus_activity_level = null;
	bio_mom_prev_births = false; bio_mom_prev_births_num = 0; 
	fet_act_lev_change = false;
	//pregnancy list of possible issues 
	bio_mom_preg_emos = false; bio_mom_preg_infects = false; 
	bio_mom_preg_premature = false; bio_mom_preg_rash = false;
	bio_mom_preg_bedrest = false; bio_mom_preg_toxemia = false; 
	bio_mom_preg_diffconc = false; bio_mom_preg_anemia = false; 
	bio_mom_preg_35lb = false; bio_mom_preg_swell = false; 
	bio_mom_preg_vagblood = false; bio_mom_preg_measles = false; 
	bio_mom_preg_nausea = false; bio_mom_preg_flu = false; 
	bio_mom_preg_hbp = false; bio_mom_preg_kidney = false; 
	bio_mom_preg_strep = false; bio_mom_preg_miscrisk = false; 
	bio_mom_preg_rh = false; bio_mom_preg_headache = false; 
	bio_mom_preg_cold = false; bio_mom_preg_urine = false; 
	bio_mom_preg_virus = false; bio_mom_preg_other = false; 
}
